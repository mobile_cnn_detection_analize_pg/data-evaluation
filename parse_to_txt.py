import os

from src.file_support import iterate_over_tests


def parse_to_txt():
    for test_path in iterate_over_tests():
        boxes_path = os.path.join(test_path, 'boxes')
        for box in os.listdir(boxes_path):
            box_path = os.path.join(boxes_path, box)
            txt_box_path = os.path.join(boxes_path, box.split('.')[0] + '.txt')
            os.rename(box_path, txt_box_path)


if __name__ == '__main__':
    parse_to_txt()
