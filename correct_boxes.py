import os

from src.file_support import iterate_over_tests


def correct_boxes():
    for test_path in iterate_over_tests():

        boxes_path = os.path.join(test_path, 'boxes')
        for box in os.listdir(boxes_path):
            box_path = os.path.join(boxes_path, box)

            with open(box_path, 'r') as f:
                file_lines = f.readlines()

            correct_lines = []
            for line in file_lines:
                split_line = line.split(" ")
                correct_name = "_".join(split_line[:-5])
                corrected_boxes = split_line[-5], split_line[-4], split_line[-1][:-1], split_line[-2], split_line[-3] + '\n'
                correct_lines.append(" ".join([correct_name] + list(corrected_boxes)))

            correct_content = "".join(correct_lines)

            with open(box_path, 'w') as f:
                f.write(correct_content)


if __name__ == '__main__':
    correct_boxes()
